<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCollectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('collect', function (Blueprint $table) {
            $table->string('part', 255)->primary();
            $table->string('manuf', 255);
            $table->string('cond', 255);
            $table->integer('price')->length(11)->unsigned();
            $table->integer('qty')->length(11)->unsigned();
            $table->string('des', 1000);
            $table->string('category', 255);
            $table->string('bin', 11);
            $table->string('arc', 11);
            $table->string('stockloc', 11);
            $table->string('short_des', 1000);
            $table->integer('purchase_price')->length(11)->unsigned();
            $table->integer('height')->length(11)->unsigned();
            $table->integer('dim_width')->length(11)->unsigned();
            $table->integer('depth')->length(11)->unsigned();
            $table->integer('tax_rate')->length(11)->unsigned();
            $table->string('def_postal_service', 255);
            $table->string('def_packaging_grp', 255);
            $table->string('is_var_parent', 11);
            $table->string('barcode_num', 255);
            $table->integer('stock_avail_lvl_loc')->length(11)->unsigned();;
            $table->integer('stock_orderbook_loc')->length(11)->unsigned();
            $table->integer('stock_val_loc')->length(11)->unsigned();
            $table->integer('stock_min_lvl_loc')->length(11)->unsigned();
            $table->integer('stock_due_loc')->length(11)->unsigned();
            $table->string('airflow', 255);
            $table->string('api', 255);
            $table->string('brand', 255);
            $table->string('bundle', 255);
            $table->string('upc', 255);
            $table->string('con', 255);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('collect');
    }
}
