<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            
            $table->string('email')->unique();
            $table->string('contact_number')->nullable();
            $table->date('birthday')->nullable();
            $table->string('address')->nullable();
            $table->string('picture')->nullable();
            $table->string('password')->nullable();
            $table->string('access_token')->nullable();
            $table->rememberToken()->nullable();
            $table->string('google_id')->nullable();
            $table->integer('is_admin')->default(0);
            $table->integer('is_manager')->default(0);
            $table->integer('is_eod')->default(0);
            $table->integer('is_flexible')->default(0);
            $table->string('schedule_in')->nullable();
            $table->string('schdule_out')->nullable();
            $table->integer('is_active')->default(1);
            $table->string('last_url')->nullable();
            $table->text('ebay_competitors_columns')->nullable();
            $table->text('ebay_items_columns')->nullable();
            $table->text('task_columns')->nullable();
            $table->text('memory_columns')->nullable();
            $table->text('cpu_module_columns')->nullable();
            $table->text('chassis_module_columns')->nullable();
            $table->text('motherboard_columns')->nullable();
            $table->text('work_orders_columns')->nullable();
            $table->text('assembly_columns')->nullable();
            $table->text('drives_columns')->nullable();
            $table->text('nebula_columns')->nullable();
            $table->timestamps();
            $table->timestamp('email_verified_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
