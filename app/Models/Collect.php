<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Collect extends Model
{
    use HasFactory;

    protected $table = 'collect';
    protected $primaryKey = 'part';
    protected $keyType = 'string';
    public $incrementing = false;
    public $timestamps = false;
}
