<?php

namespace App\Http\Controllers;

use App\Models\Collect;
use Illuminate\Http\Request;
use App\Models\Collect as CollectModel;

use FastExcel;

class CollectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('auth/login');
    }

    public function collect()
    {
        return view('collect');
    }

    public function categories(){
        return CollectModel::where('arc', 'False')
            ->orderBy('category')
            ->distinct()
            ->get('category');

    }

    public function conditions() {
        return CollectModel::where('arc', 'False')
            ->orderBy('con')
            ->distinct()
            ->get('con')
            ->map(fn($condition, $key) => $condition->con === '' ? [ 'con' => 'Blank' ] : $condition)
            ->push(['con' => 'Show Zero QTY'])
            ->all();
    }

    public function search(Request $request) {
        $searchTerm = $request->post('searchTerm');
        $paramsConditions = collect($request->post('conditions'));
        $categories = $request->post('categories');

        $qtyZeroShown = $paramsConditions->contains('Show Zero QTY');
        $conditions = $paramsConditions->filter(fn($condition, $key) => $condition !== 'Show Zero QTY');

        $hasConditions = isset($conditions) && $conditions->count() > 0;
        $hasCategories = isset($categories) && count($categories) > 0;

        $summaryResult = CollectModel::select(
            CollectModel::raw('COUNT(*) AS summaryCount'),
            CollectModel::raw('FORMAT(SUM(price * qty), 2) AS summaryTotal')
        )
            ->where('arc', 'False')
            ->when(!$qtyZeroShown, fn($query) => $query->where('qty', '>', 0))
            ->where(function($query) use ($searchTerm) {
                $query->where('part', 'LIKE', "%{$searchTerm}%")
                    ->orWhere('barcode_num' , 'LIKE', "%{$searchTerm}%")
                    ->orWhere('des' , 'LIKE', "%{$searchTerm}%");
            })
            ->when($hasConditions, function($query) use ($conditions) {
                $filters = $conditions->map(fn($condition, $key) => $condition === 'Blank' ? '' : $condition);
                $query->whereIn('con' , $filters);
            })
            ->when($hasCategories, function($query) use ($categories) {
                $query->whereIn('category', $categories);
            })
            ->first();

        $result = CollectModel::select(
            'part',
            'barcode_num',
            'des',
            'con',
            'category',
            'price',
            'qty',
            CollectModel::raw('FORMAT(price * qty, 2) AS total'),
            'bin'
        )
            ->where('arc', 'False')
            ->when(!$qtyZeroShown, fn($query) => $query->where('qty', '>', 0))
            ->where(function($query) use ($searchTerm) {
                $query->where('part', 'LIKE', "%{$searchTerm}%")
                    ->orWhere('barcode_num' , 'LIKE', "%{$searchTerm}%")
                    ->orWhere('des' , 'LIKE', "%{$searchTerm}%");
                })
            ->when($hasConditions, function($query) use ($conditions) {
                $filters = $conditions->map(fn($condition, $key) => $condition === 'Blank' ? '' : $condition);
                $query->whereIn('con' , $filters);
            })
            ->when($hasCategories, function($query) use ($categories) {
                $query->whereIn('category', $categories);
            })
            ->orderBy('total', 'DESC')
            ->paginate(100);

        $response = collect([
            'summaryCount' => $summaryResult->summaryCount,
            'summaryTotal' => $summaryResult->summaryTotal,
            'items' => $result
        ]);

        return $response;
    }

    public function exportCsv(Request $request) {
        $paramSearchTerm = $request->get('query');
        $paramConditions = $request->get('condition');
        $paramCategories = $request->get('categories');

        $searchTerm = isset($paramSearchTerm) ? $paramSearchTerm : '';
        $conditions = isset($paramConditions) ? collect(explode(',', $paramConditions)) : null;
        $categories = isset($paramCategories) ? explode(',', $paramCategories) : null;

        $qtyZeroShown = isset($conditions) && $conditions->contains('Show Zero QTY');

        if ($qtyZeroShown) {
            $conditions = $conditions->filter(fn($condition, $key) => $condition !== 'Show Zero QTY');
        }

        $hasConditions = isset($conditions) && $conditions->count() > 0;
        $hasCategories = isset($categories) && count($categories) > 0;

        $result = CollectModel::select(
            'part',
            'barcode_num',
            'des',
            'con',
            'category',
            'price',
            'qty',
            CollectModel::raw('FORMAT(price * qty, 2) AS total'),
            'bin'
        )
            ->where('arc', 'False')
            ->when(!$qtyZeroShown, fn($query) => $query->where('qty', '>', 0))
            ->where(function($query) use ($searchTerm) {
                $query->where('part', 'LIKE', "%{$searchTerm}%")
                    ->orWhere('barcode_num' , 'LIKE', "%{$searchTerm}%")
                    ->orWhere('des' , 'LIKE', "%{$searchTerm}%");
            })
            ->when($hasConditions, function($query) use ($conditions) {
                $filters = $conditions->map(fn($condition, $key) => $condition === 'Blank' ? '' : $condition);
                $query->whereIn('con' , $filters);
            })
            ->when($hasCategories, function($query) use ($categories) {
                $query->whereIn('category', $categories);
            })
            ->orderBy('total', 'DESC')
            ->get();

        $date = date_create('now')->format('Y-m-d-H-i-s');

        return FastExcel::data($result)->download("collect-{$date}.csv");
    }
}
