<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Facades\Socialite;

class GoogleController extends Controller
{
    public function loginWithGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    function dd($data){
        echo '<pre>';
        die(var_dump($data));
        echo '</pre>';
    }

    public function callbackFromGoogle()
    {
       
        try {
            $user = Socialite::driver('google')->user();
            if(explode("@", $user->email)[1] !== 'unixsurplus.com'){
                return redirect()->to('/');
            }
            // Check Users Email If Already There
            $is_user = User::where('email', $user->getEmail())->first();
            if(!$is_user){
               
                $saveUser = User::updateOrCreate([
                    'google_id' => $user->getId(),
                ],[
                    'name' => $user->getName(),
                    'email' => $user->getEmail(),
                    'picture' => $user->getAvatar(),
                    'password' => Hash::make($user->getName().'@'.$user->getId())
                ]);
            }else{
              
                $saveUser = User::where('email',  $user->getEmail())->update([
                    'google_id' => $user->getId(),
                ]);
                $saveUser = User::where('email', $user->getEmail())->first();
            }


            Auth::loginUsingId($saveUser->id);
            return redirect('dashboard');

        } catch (Exception $e) {
            return $this->sendFailedResponse($e->getMessage());
            
        }
    }
}
