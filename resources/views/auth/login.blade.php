@extends('layouts.app')

@section('content')
<style>
.btn-google {
    color: #545454;
    background-color: #ffffff;
    box-shadow: 0 1px 2px 1px #ddd;
    margin: 0;
    position: absolute;
    left: 50%;
    -ms-transform: translate(-50%);
    transform: translate(-50%);
}

.or-container {
    align-items: center;
    color: #ccc;
    display: flex;
    margin: 25px 0 ;
    
}

.line-separator {
    background-color: #ccc;
    flex-grow: 5;
    height: 1px
}

.or-label {
    flex-grow: 1;
    margin: 0 15px;
    text-align: center
}
</style>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-5 ">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                <div class="row mb-3 justify-content-center">
                    <div style="display:flex; justify-content:center; padding-bottom:15px;">
                    <img src="{{URL::asset('/img/unixsurplus.png')}}" alt="unixsurplus_logo" height="70px" width="250px">
                    </div>
                    <div style="display:flex; justify-content:center;">
                    <h3>Welcome to Intranet</h3>
                    </div>
                    </div>
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        
                        @if(Request::url() === 'http://23.90.70.20:8280')
                        <div class="row mb-3">
                            <label for="email" class="col-md-4 col-form-label text-md-end">{{ __('Email Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="password" class="col-md-4 col-form-label text-md-end">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                        <div class="or-container">
                                <div class="line-separator"></div>
                                <div class="or-label">or</div>
                                <div class="line-separator"></div>
                            </div>
                        @endif    
                            <div class="row mb-5 justify-content-center">
                                <div class="col-md-8"> <a class="btn btn-lg btn-google btn-block btn-outline" href="{{ route('google.login') }}"><img src="https://img.icons8.com/color/16/000000/google-logo.png"
                                onclick="event.preventDefault();
                                                      document.getElementById('google-form').submit();">
                                          {{ __('Login with Google') }}
                                </a> </div>
                                <form id="google-form" action="{{ route('google.login') }}" method="GET" class="d-none">
                                          @csrf
                                      </form>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
