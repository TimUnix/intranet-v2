<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
</head>
<style>
    
</style>
<body>
  <div id="app">
  <nav class="navbar navbar-light bg-light shadow-sm">
    <div class="container-fluid">
              <button class="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasNavbar" aria-controls="offcanvasNavbar">
              <span class="navbar-toggler-icon"></span>
              </button>
              <div style="margin-left:5px;"><a class="navbar-brand" href="{{ url('/') }}">{{ config('app.name', 'Intranet') }}</a></div>
                          <ul class="navbar-nav ms-auto">
                          <!-- Authentication Links -->
                          @guest
                              @if (Route::has('login'))
                                  <li class="nav-item" style="display: inline-block;">
                                      <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                  </li>
                              @endif

                              @if (Route::has('register'))
                                  <li class="nav-item" style="display: inline-block;">
                                      <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                  </li>
                              @endif
                          @else
                              <li class="nav-item dropdown">
                                  <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                      {{ Auth::user()->name }}
                                  </a>

                                  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                      <a class="dropdown-item" href="#">My Profile</a>
                                      <a class="dropdown-item" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                      document.getElementById('logout-form').submit();">
                                          {{ __('Logout') }}
                                      </a>

                                      <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                          @csrf
                                      </form>
                                  </div>
                              </li>
                          @endguest
                      </ul>
                          
  <!-- side bar -->
      <div class="offcanvas offcanvas-start" data-bs-scroll="true" tabindex="-1" id="offcanvasNavbar" aria-labelledby="offcanvasNavbarLabel">
            <div class="offcanvas-header">
              <h5 class="offcanvas-title" id="offcanvasNavbarLabel">Menu</h5>
              <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
            </div>
                    <div class="offcanvas-body">
                      <ul class="navbar-nav justify-content-end flex-grow-1 pe-3">
                        <li class="nav-item">
                          <a class="nav-link active" aria-current="page" href="#"><i class="bi bi-house"></i> Home</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="#"><i class="bi bi-table"></i> Inventory</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="{{ route('collect_view') }}"><i class="bi bi-collection"></i> Collect</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="#"><i class="bi bi-clipboard-check"></i> Task</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="#"><i class="bi bi-clipboard-data"></i> Ebay Competitors</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="#"><i class="bi bi-box-seam"></i> Ebay Items</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="#"><i class="bi bi-pie-chart"></i> Ebay Shipping</a>
                        </li>
                        <li class="nav-item dropdown">
                          <a class="nav-link dropdown-toggle" href="#" id="offcanvasNavbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                          <i class="bi bi-globe2"></i> Galactus
                          </a>
                          <ul class="dropdown-menu" aria-labelledby="offcanvasNavbarDropdown">
                            <li><a class="dropdown-item" href="#"><i class="bi bi-pc"></i> Chassis</a></li>
                            <li><a class="dropdown-item" href="#"><i class="bi bi-cpu"></i> CPU</a></li>
                            <li><a class="dropdown-item" href="#"><i class="bi bi-hdd"></i> Drives</a></li>
                            <li><a class="dropdown-item" href="#"><i class="bi bi-memory"></i> Memory</a></li>
                            <li><a class="dropdown-item" href="#"><i class="bi bi-motherboard"></i> Motherboard</a></li>
                            <li><a class="dropdown-item" href="#"><i class="bi bi-modem"></i> Switches</a></li>
                          </ul>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="#"><i class="bi bi-list-check"></i> Listing</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="#"><i class="bi bi-person-check-fill"></i> User Management</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="#"><i class="bi bi-camera"></i> Photo Request</a>
                        </li>
                      </ul>
                    </div>
      </div>
    </div>
  </nav>
  <div class="container-fluid" >
      <main class="py-4">
            @yield('content')
        </main>
        
            </div>
      </div>
</body>
<footer class="position-sticky bottom-0 bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; Your Website 2022</span>
                    </div>
                </div>
            </footer>
</html>