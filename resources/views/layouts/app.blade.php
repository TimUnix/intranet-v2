<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Intranet') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="packages/backpack/base/css/blue-bundle.css" rel="stylesheet">
</head>
<body>
    <div id="app" >
        <nav class="navbar navbar-expand-md navbar-dark bg-blue shadow-sm">
            <div class="container">
                    <ul class="navbar-nav me-auto">
                    <li class="nav-item">
                        <a class="navbar-brand" href="{{ url('/') }}">
                        <img src="/img/unixsurpluswhite.png" alt="unixsurplus_logo" height="30px" width="100px">
                        </a>
                        </li>
                    </ul>
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav ms-auto">
                        <li class="nav-item">
                          <!-- Authentication Links -->
                          @guest
                              @if (Route::has('register'))
                                  <li class="nav-item" style="display: inline-block;">
                                      <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                  </li>
                              @endif
                          @endguest
                          </li>
                      </ul>
                    <!-- Right Side Of Navbar -->
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
